jenkinsapi==0.3.7
coverage==4.5.2
pytest==4.0.1
pytest-cov==2.6.0
pytest-mock==1.10.0