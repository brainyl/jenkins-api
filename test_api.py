from datetime import datetime
import os
import sqlite3
import pytest

from api import Db, Api, main


@pytest.fixture(scope='function')
def db():
    db = Db(sqlite3.connect(":memory:"))

    yield db

    db.close()


@pytest.fixture(scope='function')
def jobs():
    jobs = [("test job", 1, str(datetime.now())), ("test job two", 0, str(datetime.now()))]

    yield jobs


@pytest.fixture(scope='function')
def server(jobs):
    class Job(object):
        def __init__(self, name, running):
            self.name = name
            self.running = running

        def is_running(self):
            return self.running

    class Server(object):
        def __init__(self, user_id=None, user_token=None, server_url=None):
            pass

        def get_jobs(self):
            return [(job[0], Job(job[0], job[1])) for job in jobs]

    server = Server()
    yield server


def test_database_is_initialized(db):
    db.initialize()
    cur = db.con.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='job'")
    assert cur.fetchone()[0] == 'job'


def test_jobs_are_saved_in_database(db, jobs):
    db.initialize()
    db.save(jobs)
    cur = db.con.execute("SELECT * from job")
    assert len(cur.fetchall()) == 2


def test_get_jobs(db, server):
    api = Api(server, db)
    assigned_jobs = api.get_jobs()

    assert len(assigned_jobs) == 2


def test_api_can_save_jobs(db, server, jobs):
    api = Api(server, db)
    api.save_jobs(jobs)
    cur = db.con.execute("SELECT * from job")
    assert len(cur.fetchall()) == 2


def test_main(mocker):
    m = mocker.patch('api.Api.save_jobs')
    main()
    m.assert_called_once()