from jenkinsapi.jenkins import Jenkins
import os
import sqlite3
from datetime import datetime


class Api(object):
    def __init__(self, server, db):
        self.server = server
        self.db = db

    def get_jobs(self):
        running_jobs = []
        for job_name, job_instance in self.server.get_jobs():
            job = (job_instance.name, int(job_instance.is_running()), str(datetime.now()))
            running_jobs.append(job)
        return running_jobs

    def save_jobs(self, jobs):
        self.db.save(jobs)


class Db(object):
    def __init__(self, con):
        self.con = con
        self.initialize()

    def initialize(self):
        self.con.execute("drop table if exists job")
        self.con.execute(
            "create table job (name varchar unique primary key, status integer, time_checked text)")
        return self.con

    def save(self, jobs):
        with self.con:
            self.con.executemany('INSERT INTO job VALUES (?,?,?)', jobs)
            return self.con

    def close(self):
        self.con.close()


def main():
    db = Db(sqlite3.connect("jenkins.db"))
    try:
        JENKINS_USERID = os.environ['JENKINS_USERID']
        JENKINS_TOKEN = os.environ['JENKINS_TOKEN']
        JENKINS_ADDR = os.environ['JENKINS_ADDR']
        server = Jenkins(JENKINS_ADDR, username=JENKINS_USERID, password=JENKINS_TOKEN)
        api = Api(server, db)
        jobs = api.get_jobs()
        api.save_jobs(jobs)
    except KeyError:
        print("Please set the necessary environment variables")
    finally:
        db.close()


if __name__ == '__main__':
    main()
